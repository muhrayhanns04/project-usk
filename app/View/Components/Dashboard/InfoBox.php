<?php

namespace App\View\Components\Dashboard;

use Illuminate\View\Component;

class InfoBox extends Component
{
    public $value;
    public $title;
    public $icon;
    public $bg;
    public $type;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($value, $title, $icon, $bg, $type)
    {
        $this->value = $value;
        $this->title = $title;
        $this->icon = $icon;
        $this->bg = $bg;
        $this->type = $type;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.dashboard.info-box');
    }
}
