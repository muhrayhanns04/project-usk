<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MajorTeacher extends Model
{
    use HasFactory;
    protected $fillable = ["grade_id", "name", "user_id"];
}
