<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = ["image", "name", "user_id", "description", "price"];

    function user()
    {
        return $this->belongsTo(User::class);
    }
}
