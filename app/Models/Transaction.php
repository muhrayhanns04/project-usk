<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    function user()
    {
        return $this->belongsTo(User::class);
    }
    function confirmedBy()
    {
        return $this->belongsTo(User::class, 'confirmed_by', 'id');
    }

    function withdrawal()
    {
        return $this->belongsTo(Withdrawal::class);
    }
    function order()
    {
        return $this->belongsTo(Order::class);
    }
}
