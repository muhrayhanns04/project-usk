<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        if (Auth::user()->level === "merchant") {
        }
        $wallet = Wallet::where("user_id", Auth::user()->id)->first();
        $students = User::where("level", "student")->get();
        $merchant = User::where("level", "merchant")->get();
        $bank = Bank::findOrFail(1);

        return view('pages.guest.home', compact('students', 'merchant', 'bank', 'wallet'));
    }
}
