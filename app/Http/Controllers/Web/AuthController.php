<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class AuthController extends Controller
{
    public function registerView()
    {
        return view('auth.register');
    }

    public function check(Request $request)
    {
        $request->validate([
            'registration_number' => 'required',
            'password' => 'required|string',
            // 'role_id' => 'required'
        ]);

        if (Auth::attempt(['nss' => $request->registration_number, 'password' => $request->password])) {
            return redirect("/dashboard");
        } else if (Auth::attempt(['nibs' => $request->registration_number, 'password' => $request->password])) {
            return redirect("/dashboard");
        } else if (Auth::attempt(['nib' => $request->registration_number, 'password' => $request->password])) {
            return redirect("/dashboard");
        }

        return back()->withErrors(["Akun tidak cocok"])
            ->withInput();
    }

    public function loginView()
    {
        return view('auth.login');
    }

    public function logout()
    {
        Auth::logout();
        Alert::success('Keluar', 'Sampai jumpa!');
        return redirect('/');
    }
}
