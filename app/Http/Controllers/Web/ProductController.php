<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $title = "Produk";
        $products = Product::where("user_id", Auth::user()->id)->get();
        return view('pages.admin.store.product', compact("title", "products"));
    }

    public function store(Request $request)
    {
        $product = new Product;
        $product->user_id = Auth::user()->id;
        $product->image = $request->image;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->description = $request->description;
        $product->save();

        Alert::toast('📦 Berhasil membuat product ' . $request->name, 'success');
        return back();
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->user_id = Auth::user()->id;
        $product->image = $request->image;
        $product->name = $request->name;
        $product->stock = $request->stock;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->update();

        Alert::toast('📦 Berhasil update product ' . $request->name, 'success');
        return back();
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        Alert::toast('Berhasil menghapus produk ' . $product->name, 'success');
        $product->delete();

        return back();
    }
}
