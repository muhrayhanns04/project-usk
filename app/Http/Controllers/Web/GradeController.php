<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Grade;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;
use RealRashid\SweetAlert\Facades\Alert;

class GradeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('search')) {
            $grades = Grade::where('name', 'LIKE', '%' . $request->search . '%')->paginate(150);
        } else {
            $grades = Grade::paginate(20);
        }

        $title = "Kelas";
        return view('pages.admin.grade.index', compact("title", "grades"));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
        ]);

        $grade = new Grade;
        $grade->name = $request->name;
        $grade->save();

        Alert::toast('Berhasil membuat kelas ' . $request->name, 'success');
        return back();
    }

    public function update(Request $request, $id)
    {
        $grade = Grade::findOrFail($id);
        $grade->name = $request->name;
        $grade->update();

        Alert::toast('Berhasil edit kelas ' . $grade->name, 'success');
        return back();
    }

    public function destroy($id)
    {
        $grade = Grade::findOrFail($id);
        $grade->delete();

        Alert::toast('Berhasil menghapus kelas ' . $grade->name, 'success');
        return back();
    }
}
