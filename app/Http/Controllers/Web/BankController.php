<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class BankController extends Controller
{
    public function index(Request $request)
    {
        $title = "Akun Bank";
        $users = User::where("level", "bank")->get();
        return view('pages.admin.bank.index', compact("title", "users"));
    }

    public function store(Request $request)
    {
        $findNibs = User::where("level", "bank")->where("nibs", $request->nibs)->first();
        $findEmail = User::where("level", "bank")->where("email", $request->email)->first();

        if ($findNibs) {
            Alert::toast('Gagal NIBS sudah terdaftarkan ' . $request->nibs, 'error');
            return back();
        }

        if ($findEmail) {
            Alert::toast('Gagal Email sudah terdaftarkan ' . $request->email, 'error');
            return back();
        }

        $user = new User;
        $user->nibs = $request->nibs;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->level = "bank";
        $user->password = Hash::make("siswasmkn10");
        $user->save();

        Alert::toast('Berhasil membuat bank ' . $request->name, 'success');
        return back();
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->nibs = $request->nibs;
        $user->name = $request->name;
        $user->email = $request->email;
        if ($user->password !== $request->password) {
            $user->password = Hash::make($request->password);
        }
        $user->update();

        Alert::toast('Berhasil update bank ' . $request->name, 'success');
        return back();
    }

    public function destroy($id)
    {
        $user = User::where("nibs", $id)->first();
        Alert::toast('Berhasil menghapus bank ' . $user->name, 'success');
        $user->delete();

        return back();
    }
}
