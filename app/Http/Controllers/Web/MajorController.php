<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Grade;
use App\Models\Major;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class MajorController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('search')) {
            $majors = Major::where('name', 'LIKE', '%' . $request->search . '%')->paginate(150);
        } else {
            $majors = Major::paginate(100);
        }

        $grades = Grade::all();

        $title = "Jurusan";
        return view('pages.admin.major.index', compact("title", "grades", "majors"));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'grade_id' => 'required'
        ]);

        if ($validator->fails()) {
            Alert::toast('Gagal membuat jurusan', 'error');
            return redirect("/settings/major");
        }

        if (strlen($request->grade_id) < 1) {
            Alert::toast('Gagal membuat jurusan', 'error');
            return redirect("/settings/major");
        }

        $major = new Major;
        $major->name = $request->name;
        $major->grade_id = $request->grade_id;
        $major->save();

        Alert::toast('Berhasil membuat jurusan ' . $request->name, 'success');
        return back();
    }

    public function update(Request $request, $id)
    {
        $major = Major::findOrFail($id);
        $major->name = $request->name;
        $major->grade_id = $request->grade_id;
        $major->update();

        Alert::toast('Berhasil edit jurusan ' . $major->name, 'success');
        return back();
    }

    public function destroy($id)
    {
        $major = Major::findOrFail($id);
        $major->delete();

        Alert::toast('Berhasil menghapus jurusan ' . $major->name, 'success');
        return back();
    }
}
