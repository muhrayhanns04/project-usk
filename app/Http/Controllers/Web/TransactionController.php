<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Barryvdh\DomPDF\Facade as PDF;

class TransactionController extends Controller
{
    public function download($id)
    {
        $item = Transaction::find($id);
        $date = Carbon::parse($item->created_at)->isoFormat('dddd, Do MMMM YYYY');
        $filedate =
            Carbon::parse($item->created_at)->isoFormat('d/M/Y');

        $file = PDF::loadView('pages.invoice.template', compact('item'))->setOptions(['defaultFont' => 'Poppins', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);
        return $file->download($item->invoice_id . "-" . $item->user->name . "-" . $filedate . ".pdf");
    }

    public function showReport(Request $request, $id, $status)
    {        
        if($status === "rejected") {
            $transactions = Transaction::where("type", 3)->where("confirmed_by", Auth::user()->id)->where("status", 1)->where("product_status",  0)->where("invoice_id", $id)->orderBy('created_at', 'desc')->get();
        } else {
            $transactions = Transaction::where("type", 3)->where("confirmed_by", Auth::user()->id)->where("status", 2)->where("product_status",  0)->where("invoice_id", $id)->orderBy('created_at', 'desc')->get();       
        }

        $total_payment = 0;
        foreach ($transactions as $item) {
            $total_payment += $item->order->total;
        }

        return view('pages.invoice.invoice-product', compact('transactions', 'total_payment'));
    }

    public function downloadReport(Request $request, $id, $status)
    {        
        if($status === "rejected") {
            $transactions = Transaction::where("type", 3)->where("confirmed_by", Auth::user()->id)->where("status", 1)->where("product_status",  0)->where("invoice_id", $id)->orderBy('created_at', 'desc')->get();
        } else {
            $transactions = Transaction::where("type", 3)->where("confirmed_by", Auth::user()->id)->where("status", 2)->where("product_status",  0)->where("invoice_id", $id)->orderBy('created_at', 'desc')->get();       
        }

        $total_payment = 0;
        foreach ($transactions as $item) {
            $total_payment += $item->order->total;
        }

        $pdf = PDF::loadView('pages.invoice.invoice-product', compact('transactions', 'total_payment'))->setOptions(['defaultFont' => 'Inter', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);;
        return $pdf->download($transactions[0]->invoice_id . '.pdf');
    }

    public function index(Request $request)
    {
        $transactions = Transaction::all();
        $title = "Topup";
        $users = User::where("level", "student")->get();
        return view('pages.admin.transaction.index', compact("title", "transactions", "users"));
    }

    public function pendingView(Request $request)
    {
        $title = "Transaksi";

        $data = Transaction::where("type", 3)->where("confirmed_by", Auth::user()->id)->where("status", 0)->where("product_status",  0)->orderBy('created_at', 'desc')->get();
        $transactions = collect($data)->groupBy("invoice_id");

        return view('pages.admin.transaction.product_transaction.group.pending', compact("title", "transactions"));
    }

    public function detailPendingView(Request $request, $invoice_id)
    {
        $title = "Transaksi";

        $transactions = Transaction::where("type", 3)->where("confirmed_by", Auth::user()->id)->where("status", 0)->where("product_status",  0)->where("invoice_id", $invoice_id)->orderBy('created_at', 'desc')->get();

        return view('pages.admin.transaction.product_transaction.index', compact("title", "transactions"));
    }

    public function rejectedView(Request $request)
    {
        $title = "Transaksi Ditolak";
        $data = Transaction::where("type", 3)->where("confirmed_by", Auth::user()->id)->where("status", 1)->orderBy('created_at', 'desc')->get();
        $transactions = collect($data)->groupBy("invoice_id");
        return view('pages.admin.transaction.product_transaction.group.rejected', compact("title", "transactions"));
    }
    public function detailRejectedView(Request $request, $invoice_id)
    {
        $title = "Transaksi Ditolak";
        $transactions = Transaction::where("type", 3)->where("confirmed_by", Auth::user()->id)->where("status", 1)->where("invoice_id", $invoice_id)->orderBy('created_at', 'desc')->get();
        return view('pages.admin.transaction.product_transaction.rejected', compact("title", "transactions"));
    }

    public function successView(Request $request)
    {
        $title = "Transaksi Berhasil";
        $data = Transaction::where("type", 3)->where("confirmed_by", Auth::user()->id)->where("status", 2)->orderBy('created_at', 'desc')->get();
        $transactions = collect($data)->groupBy("invoice_id");
        return view('pages.admin.transaction.product_transaction.group.success', compact("title", "transactions"));
    }
    public function detailSuccessView(Request $request, $invoice_id)
    {
        $transactions = Transaction::where("type", 3)->where("confirmed_by", Auth::user()->id)->where("status", 2)->where("invoice_id", $invoice_id)->orderBy('created_at', 'desc')->get();
        $title = "Transaksi Berhasil";
        return view('pages.admin.transaction.product_transaction.success', compact("title", "transactions"));
    }

    public function approve($id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->status = 2;
        $transaction->product_status = 0;
        $transaction->updated_at = Carbon::now();
        $transaction->confirmed_by = Auth::user()->id;
        $transaction->update();

        Alert::toast('Berhasil konfirmasi transaksi produk sebesar ' . "Rp" . number_format($transaction->order->total, 0, '.', ',') . ", Atas nama " .  $transaction->user->name, 'success');
        return back();
    }

    public function approveAll($invoice_id)
    {
        $transactions = Transaction::where("type", 3)->where("confirmed_by", Auth::user()->id)->where("status", 0)->where("product_status",  0)->where("invoice_id", $invoice_id)->orderBy('created_at', 'desc')->get();

        foreach ($transactions as $item) {
            $transaction = Transaction::findOrFail($item->id);
            $transaction->status = 2;
            $transaction->product_status = 0;
            $transaction->updated_at = Carbon::now();
            $transaction->confirmed_by = Auth::user()->id;
            $transaction->update();
        }

        Alert::toast('Berhasil konfirmasi transaksi produk Invoice ID ' . $invoice_id, 'success');
        return back();
    }

    public function reject($id)
    {
        // change transaction status
        $transaction = Transaction::findOrFail($id);
        $transaction->status = 1;
        $transaction->product_status = 0;
        $transaction->updated_at = Carbon::now();
        $transaction->confirmed_by = Auth::user()->id;
        $transaction->update();

        // refund product quantity
        $product = Product::findOrFail($transaction->order->product->id);
        $product->stock = $product->stock + $transaction->order->qty;
        $product->update();

        // refund buyer
        $buyerWallet = Wallet::where('user_id', $transaction->user_id)->first();
        $buyerWallet->balance = $buyerWallet->balance + $transaction->order->total;
        $buyerWallet->update();

        // reduce seller balance
        $merchantWallet = Wallet::where('user_id', Auth::user()->id)->first();
        $merchantWallet->balance = $merchantWallet->balance - $transaction->order->total;
        $merchantWallet->update();

        Alert::toast('Berhasil membatalkan transaksi produk sebesar ' . "Rp" . number_format($transaction->order->total, 0, '.', ',') . ", Atas nama " .  $transaction->user->name, 'success');
        return back();
    }

    public function unrejected($id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->status = 1;
        $transaction->product_status = 0;
        $transaction->updated_at = Carbon::now();
        $transaction->confirmed_by = Auth::user()->id;
        $transaction->update();

        $product = Product::findOrFail($transaction->order->product->id);
        $product->stock = $product->stock + $transaction->order->qty;
        $product->update();

        $merchantWallet = Wallet::where('user_id', Auth::user()->id)->first();
        $merchantWallet->balance = $merchantWallet->balance - $transaction->order->total;
        $merchantWallet->update();

        $buyerWallet = Wallet::where('user_id', $transaction->user_id)->first();
        $buyerWallet->balance = $buyerWallet->balance + $transaction->order->total;
        $buyerWallet->update();

        Alert::toast('Berhasil membatalkan transaksi produk sebesar ' . "Rp" . number_format($transaction->order->total, 0, '.', ',') . ", Atas nama " .  $transaction->user->name, 'success');
        return back();
    }

}
