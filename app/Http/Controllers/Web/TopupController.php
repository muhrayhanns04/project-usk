<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Barryvdh\DomPDF\Facade as PDF;

class TopupController extends Controller
{
    public function showReport(Request $request, $id)
    {
        $item = Transaction::find($id);
        return view('pages.invoice.template', compact('item'));
    }

    public function reportByFilter(Request $request)
    {
        $transactions = Transaction::where("type", 0)->where("status", 2)->orderBy('created_at', 'desc')->whereDate("created_at", Carbon::today())->get();

        $total_payment = 0;
        foreach ($transactions as $item) {
            $total_payment += $item->cash_request;
        }

        
        $pdf = PDF::loadView('pages.invoice.invoice-topups', compact("transactions", 'total_payment'))->setOptions(['defaultFont' => 'Inter', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);
        return $pdf->download("Report Today.pdf");

    }

    public function downloadReport(Request $request, $id)
    {
        $item = Transaction::find($id);

        $pdf = PDF::loadView('pages.invoice.template', compact("item"))->setOptions(['defaultFont' => 'Inter', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);;
        return $pdf->download($item->invoice_id . '.pdf');
    }

    public function index(Request $request)
    {
        $transactions = Transaction::where("type", 0)->where("status", 0)->orderBy('created_at', 'desc')->get();
        $title = "Topup";
        $users = User::where("level", "student")->get();
        $merchants = User::where("level", "merchant")->get();
        return view('pages.admin.transaction.topup.index', compact("title", "transactions", "users", "merchants"));
    }

    public function rejectedView(Request $request)
    {
        $transactions = Transaction::where("type", 0)->where("status", 1)->orderBy('created_at', 'desc')->get();
        $title = "Topup Ditolak";
        $users = User::where("level", "student")->get();
        $merchants = User::where("level", "merchant")->get();
        return view('pages.admin.transaction.topup.rejected', compact("title", "transactions", "users", "merchants"));
    }

    public function successView(Request $request)
    {
        $transactions = Transaction::where("type", 0)->where("status", 2)->orderBy('created_at', 'desc')->get();
        $title = "Topup Berhasil";
        $users = User::where("level", "student")->get();
        $merchants = User::where("level", "merchant")->get();
        return view('pages.admin.transaction.topup.success', compact("title", "transactions", "users", "merchants"));
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'cash_request' => 'required|string',
        ]);

        if ($request->user_id == 0) {
            Alert::toast('Gagal topup sebesar ' . "Rp" . number_format($request->cash_request, 0, '.', ',') . "User tidak ditemukan", 'error');
            return back();
        }

        $user = User::findOrFail($request->user_id);

        $transaction = new Transaction;
        $transaction->invoice_id = "INVTP" . $user->nisn  . now()->timestamp;
        $transaction->user_id  = $request->user_id;
        $transaction->cash_request  = $request->cash_request;
        $transaction->type  = 0;
        $transaction->status  = 2;
        $transaction->created_at = Carbon::now();
        $transaction->updated_at = Carbon::now();
        $transaction->confirmed_by = Auth::user()->id;
        $transaction->save();

        $wallet = Wallet::where('user_id', $request->user_id)->first();
        $wallet->balance = $wallet->balance + $request->cash_request;
        $wallet->update();

        $bank = Bank::findOrFail(1);
        $bank->balanced = $bank->balanced + $request->cash_request;
        $bank->topup = $bank->topup + $request->cash_request;
        $bank->update();

        Alert::toast('Berhasil topup sebesar ' . "Rp" . number_format($request->cash_request, 0, '.', ',') . ", Atas nama " .  $user->name, 'success');
        return redirect()->route("topup.success");
    }

    public function approve($id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->status = 2;
        $transaction->updated_at = Carbon::now();
        $transaction->confirmed_by = Auth::user()->id;
        $transaction->update();

        $wallet = Wallet::where('user_id', $transaction->user_id)->first();
        $wallet->balance = $wallet->balance + $transaction->cash_request;
        $wallet->update();

        $bank = Bank::findOrFail(1);
        $bank->balanced = $bank->balanced + $transaction->cash_request;
        $bank->topup = $bank->topup + $transaction->cash_request;
        $bank->update();

        Alert::toast('Berhasil konfirmasi topup sebesar ' . "Rp" . number_format($transaction->cash_request, 0, '.', ',') . ", Atas nama " .  $transaction->user->name, 'success');
        return back();
    }
    public function reject($id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->status = 1;
        $transaction->updated_at = Carbon::now();
        $transaction->confirmed_by = Auth::user()->id;
        $transaction->update();

        Alert::toast('Berhasil membatalkan topup sebesar ' . "Rp" . number_format($transaction->cash_request, 0, '.', ',') . ", Atas nama " .  $transaction->user->name, 'success');
        return back();
    }
}
