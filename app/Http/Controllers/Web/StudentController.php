<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Major;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class StudentController extends Controller
{
    public function index(Request $request)
    {
        $title = "Siswa";
        $users = User::where("level", "student")->get();
        $majors = Major::all();
        return view('pages.admin.students.index', compact("title", "users", "majors"));
    }

    public function store(Request $request)
    {
        $findNisn = User::where("level", "student")->where("nisn", $request->nisn)->first();
        $findEmail = User::where("level", "student")->where("email", $request->email)->first();

        if ($findNisn) {
            Alert::toast('Gagal NISN sudah terdaftarkan ' . $request->nisn, 'error');
            return back();
        }

        if ($findEmail) {
            Alert::toast('Gagal Email sudah terdaftarkan ' . $request->email, 'error');
            return back();
        }

        $user = new User;
        $user->nisn = $request->nisn;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->major_id = $request->major_id;
        $user->password = Hash::make("siswasmkn10");
        $user->save();

        $wallet = new Wallet;
        $wallet->user_id = $user->id;
        $wallet->balance = 0;
        $wallet->income = 0;
        $wallet->save();

        Alert::toast('Berhasil membuat akun siswa ' . $request->name, 'success');
        return back();
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->nisn = $request->nisn;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->major_id = $request->major_id;
        if ($user->password !== $request->password) {
            $user->password = Hash::make($request->password);
        }
        $user->update();

        Alert::toast('Berhasil update akun siswa ' . $request->name, 'success');
        return back();
    }

    public function destroy($id)
    {
        $user = User::where("nisn", $id)->first();
        Alert::toast('Berhasil menghapus akun siswa ' . $user->name, 'success');
        $user->delete();

        return back();
    }
}
