<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class StoreController extends Controller
{
    public function viewAccount(Request $request)
    {
        $title = "Akun Toko";
        $users = User::where("level", "merchant")->get();
        return view('pages.admin.store.account', compact("title", "users",));
    }

    public function accountStore(Request $request)
    {
        $findNib = User::where("level", "merchant")->where("nib", $request->nib)->first();
        $findEmail = User::where("level", "merchant")->where("email", $request->email)->first();

        if ($findNib) {
            Alert::toast('Gagal NIB sudah terdaftarkan ' . $request->nib, 'error');
            return back();
        }

        if ($findEmail) {
            Alert::toast('Gagal Email sudah terdaftarkan ' . $request->email, 'error');
            return back();
        }

        $user = new User;
        $user->nib = $request->nib;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->level = "merchant";
        $user->password = Hash::make("tokosmkn10");
        $user->save();

        $wallet = new Wallet;
        $wallet->user_id = $user->id;
        $wallet->balance = 0;
        $wallet->income = 0;
        $wallet->save();

        Alert::toast('Berhasil membuat akun toko ' . $request->name, 'success');
        return back();
    }

     public function accountUpdate(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->nib = $request->nib;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->update();

        Alert::toast('Berhasil update akun toko ' . $request->name, 'success');
        return back();
    }

    public function accountDestroy($id)
    {
        $user = User::where("nib", $id)->first();
        Alert::toast('Berhasil menghapus akun toko ' . $user->name, 'success');
        $user->delete();

        return back();
    }
}
