<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class TopupController extends Controller
{
    public function index($id)
    {
        $status = request("status");
        $transactions = Transaction::where("user_id", $id)->where("type", 0)->where("status", $status)->orderBy('created_at', 'desc')->get();
        return response()->json(['message' => "Ok", 'result' => $transactions], Response::HTTP_CREATED);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cash_request' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Input tidak valid', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }

        $transaction = new Transaction;
        $transaction->invoice_id = "INVTP" . $request->user()->nisn  . now()->timestamp;
        $transaction->user_id  = $request->user()->id;
        $transaction->cash_request  = $request->cash_request;
        $transaction->type  = 0;
        $transaction->status  = 0;
        $transaction->created_at = Carbon::now();
        $transaction->updated_at = Carbon::now();
        $transaction->save();

        return response()->json(['message' => 'Menunggu konfirmasi topup sebesar ' . "Rp" . number_format($request->cash_request, 0, '.', ',') . ", Atas nama " .  $request->user()->name,], Response::HTTP_CREATED);
    }
}
