<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CartResource;
use App\Http\Resources\ProductTransactionResource;
use App\Models\Order;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Barryvdh\DomPDF\Facade as PDF;

class TransactionController extends Controller
{
    public function download($id)
    {
        $item = Transaction::find($id);
        $date = Carbon::parse($item->created_at)->isoFormat('dddd, Do MMMM YYYY');
        $filedate =
            Carbon::parse($item->created_at)->isoFormat('d/M/Y');

        $file = PDF::loadView('pages.invoice.template', compact('item', 'date'))->setOptions(['defaultFont' => 'Inter', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);
        return $file->download($item->invoice_id . "-" . $item->user->name . "-" . $filedate);
    }

    public function index(Request $request)
    {
        $status = request("status");
        $transactions = Transaction::where("user_id", $request->user()->id)->where("type", 3)->where("status", $status)->where("product_status", 0)->orderBy('created_at', 'desc')->get();
        return response()->json(['message' => "Ok", 'result' => ProductTransactionResource::collection($transactions)], Response::HTTP_CREATED);
    }

    public function addToCart(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'qty' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Input tidak valid', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }

        // check product on cart & get product by id
        $product = Product::find($id);
        $onCartCheck = Transaction::select("transactions.*")->where("transactions.product_status", 1)->join("orders", "orders.id", "=", "transactions.order_id")->where("orders.product_id", $product->id)->first();

        // if product ready on cart
        if ($onCartCheck && $product->stock >= $request->qty) {
            // Reduce the number of stock products
            $product->stock = $product->stock - $request->qty;
            $product->update();

            // Update quantity
            $order = Order::findOrFail($onCartCheck->order_id);
            $order->qty = $order->qty + $request->qty;
            $order->total = $product->price * ($order->qty);
            return $order->update();
        }

        if (!$product) {
            return response()->json(['message' => 'Produk tidak ditemukan', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }

        if ($request->user()->level == "merchant") {
            return response()->json(['message' => 'Pedagang tidak bisa membeli produk'], Response::HTTP_EXPECTATION_FAILED);
        }

        if ($product->stock < $request->qty) {
            return response()->json(['message' => 'Stok barang tidak mencukupi'], Response::HTTP_UNAUTHORIZED);
        }

        if ($product->stock >= $request->qty) {
            // Reduce the number of stock products
            $product->stock = $product->stock - $request->qty;
            $product->update();

            // Make a new order
            $order = new Order;
            $order->user_id = $request->user()->id;
            $order->product_id = $product->id;
            $order->qty = $request->qty;
            $order->total = $product->price * $request->qty;
            $order->save();

            // Make a new transaction
            $transaction = new Transaction;
            $transaction->user_id  = $request->user()->id;
            $transaction->type  = 3; //order
            $transaction->status  = 0; //pending on cart
            $transaction->order_id  = $order->id;
            $transaction->product_status  = 1;
            $transaction->created_at = Carbon::now();
            $transaction->confirmed_by = $product->user->id;
            $transaction->save();
        }

        return response()->json(['message' => 'Ok'], Response::HTTP_CREATED);
    }

    // Old function
    public function addToCartV2(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'qty' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'Input tidak valid', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }

        $product = Product::find($id);

        if (!$product) {
            return response()->json(['message' => 'Produk tidak ditemukan', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }
        if ($request->user()->level == "merchant") {
            return response()->json(['message' => 'Pedagang tidak bisa membeli produk'], Response::HTTP_EXPECTATION_FAILED);
        }
        if ($product->stock < $request->qty) {
            return response()->json(['message' => 'Stok barang tidak mencukupi'], Response::HTTP_UNAUTHORIZED);
        }
        if ($product->stock >= $request->qty) {
            $product->stock = $product->stock - $request->qty;
            $product->update();

            $order = new Order;
            $order->user_id = $request->user()->id;
            $order->product_id = $product->id;
            $order->qty = $request->qty;
            $order->total = $product->price * $request->qty;
            $order->save();

            // transaction
            $transaction = new Transaction;
            $transaction->user_id  = $request->user()->id;
            $transaction->type  = 3; //order
            $transaction->status  = 0; //pending on cart
            $transaction->order_id  = $order->id;
            $transaction->product_status  = 1;
            $transaction->created_at = Carbon::now();
            $transaction->confirmed_by = $product->user->id;
            $transaction->save();
        }
        return response()->json(['message' => 'Ok'], Response::HTTP_CREATED);
    }

    public function onCart(Request $request)
    {
        $transactions = Transaction::where("user_id", $request->user()->id)->where("type", 3)->where("status", 0)->where("product_status", 1)->orderBy('created_at', 'desc')->get();
        return response()->json(['message' => "Ok", 'result' => CartResource::collection($transactions)], Response::HTTP_CREATED);
    }

    public function cartCount(Request $request)
    {
        $transactions = Transaction::where("user_id", $request->user()->id)->where("type", 3)->where("status", 0)->where("product_status", 1)->get();

        $total_quantity = 0;
        foreach ($transactions as $item) {
            $total_quantity += $item->order->qty;
        }

        return response()->json(['message' => "Ok", 'result' => $total_quantity], Response::HTTP_CREATED);
    }

    public function checkout(Request $request)
    {
        $transactions = Transaction::where("user_id", $request->user()->id)->where("type", 3)->where("status", 0)->where("product_status", 1)->orderBy('created_at', 'desc');

        $total_payment = 0;
        foreach ($transactions->get() as $item) {
            $total_payment += $item->order->total;
        }

        $wallet = Wallet::where("user_id", $request->user()->id)->first();

        if ($total_payment == 0) {
            return response()->json(['message' => 'Silahkan tambahkan barang ke dalam keranjang'], Response::HTTP_UNAUTHORIZED);
        }

        if ($wallet->balance < $total_payment) {
            return response()->json(['message' => 'Saldo tidak mencukupi', 'balanced' => $wallet->balance, 'total_payment' => $total_payment], Response::HTTP_UNAUTHORIZED);
        }

        $wallet->balance = $wallet->balance - $total_payment;
        $wallet->updated_at = Carbon::now();
        $wallet->update();

        $merchant_code = "";

        foreach ($transactions->get() as $item) {
            $merchant_code = $item->order->product->user->nib;
            $merchantWallet = Wallet::where("user_id", $item->order->product->user->id)->first();
            $merchantWallet->balance = $merchantWallet->balance + $item->order->total;
            $merchantWallet->updated_at = Carbon::now();
            $merchantWallet->update();
        }

        $transactions->update([
            'invoice_id' => "INVT" . "-" . $merchant_code . "-" . now()->timestamp,
            'product_status' => 0
        ]);

        return response()->json(['message' => "Ok", 'payment_detail' => [
            'invoice' => "INVT" . $merchant_code  . now()->timestamp,
            'total' => $total_payment,
            'created_at' => Carbon::now()->format("d-m-Y")
        ]], Response::HTTP_CREATED);
    }

    public function totalPayment(Request $request)
    {
        $transactions = Transaction::where("user_id", $request->user()->id)->where("type", 3)->where("status", 0)->where("product_status", 1)->orderBy('created_at', 'desc');

        $total_payment = 0;
        foreach ($transactions->get() as $item) {
            $total_payment += $item->order->total;
        }

        return response()->json(['message' => "Ok, total", 'result' => $total_payment], Response::HTTP_CREATED);
    }

    public function updateQuantity(Request $request)
    {
        $type = request('type');
        $order = Order::whereDate('created_at', Carbon::today())->where("user_id", $request->user()->id)->first();
        $product = Product::find($order->product_id);

        if (!$order) {
            return response()->json(['message' => 'Barang tidak ditemukan'], Response::HTTP_UNAUTHORIZED);
        }

        if ($type == 1) {
            $order->qty = $order->qty + 1;
            $product->stock = $product->stock - 1;
        } else {
            $order->qty = $order->qty - 1;
            $product->stock = $product->stock + 1;
        }
        $product->update();
        $order->update();
        return response()->json(['message' => "Ok", 'product' => $order], Response::HTTP_CREATED);
    }

    public function destroy($id)
    {
        $transaction = Transaction::findOrFail($id);

        $product = Product::find($transaction->order->product->id);
        $product->stock = $product->stock + $transaction->order->qty;
        $product->update();

        $transaction->delete();

        return response()->json(['message' => "Ok", 'product' => $transaction], Response::HTTP_CREATED);
    }
}
