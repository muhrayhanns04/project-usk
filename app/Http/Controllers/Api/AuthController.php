<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function index(Request $request)
    {
        $results = $request->user();
        return response()->json(['message' => 'Berhasil mendapatkan user', 'results' => $results], Response::HTTP_OK);
    }
    public function login(Request $request)
    {
        if ($request->level === "student") {
            if (strlen($request->registration_number) < 1) {
                return response()->json(['message' => 'NISN tidak ditemukan'], Response::HTTP_EXPECTATION_FAILED);
            }
            $user = User::where("nisn", $request->registration_number)->first();
        } else if ($request->level === "merchant") {
            if (strlen($request->registration_number) < 1) {
                return response()->json(['message' => 'NIB tidak ditemukan'], Response::HTTP_EXPECTATION_FAILED);
            }
            $user = User::where("nib", $request->registration_number)->first();
        }

        if (!$user) {
            return response()->json(['message' => 'Akun tidak ditemukan, silahkan register terlebih dahulu 😊'], Response::HTTP_EXPECTATION_FAILED);
        }

        if (!$user || !Hash::check($request->password, $user->password)) {
            return response()->json(['message' => 'Password tidak sesuai'], Response::HTTP_UNAUTHORIZED);
        }

        $token = $user->createToken('@hansbillfoldindonesia')->plainTextToken;
        return response()->json(['message' => 'Ok', 'token' => $token, 'token_type' => 'Bearer Token', 'results' => $user], Response::HTTP_OK);
    }

    public function register(Request $request)
    {
        if ($request->level === "student") {
            $validator = Validator::make($request->all(), [
                'nisn' => 'required|string|max:10|min:10',
                'password' => 'required|string',
                'name' => "required|string|max:35|min:4",
                'major_id' => "required",
                'level' => "required"
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'nign' => 'required|string|max:10|min:10',
                'name' => "required|string|max:35|min:4",
                'password' => 'required|string',
                'level' => "required"
            ]);
        }

        if ($validator->fails()) {
            return response()->json(['message' => 'Input tidak valid', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }
        if ($request->level === "student") {
            $usernameFinded = User::where("nisn", $request->nisn)->first();
        } else {
            $usernameFinded = User::where("nign", $request->nign)->first();
        }

        if ($usernameFinded) {
            return response()->json(['message' =>  "Data sudah terdaftar, silahkan masuk"], Response::HTTP_FOUND);
        }

        if ($request->level === "student") {
            $user = new User();
            $user->nisn = $request->nisn;
            $user->name = $request->name;
            $user->password = Hash::make($request->password);
            $user->major_id = $request->major_id;
            $user->level = "student";
            $user->login_type = "nonsso";
            $user->save();
        } else {
            $user = new User();
            $user->nign = $request->nisn;
            $user->name = $request->name;
            $user->password = Hash::make($request->password);
            $user->level = $request->level;
            $user->login_type = "nonsso";
            $user->save();
        }

        return response()->json(['message' => 'Berhasil mendaftarkan sebagai ' . $user->level, 'results' => $user], Response::HTTP_CREATED);
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->currentAccessToken()->delete();

        return response()->json([
            'message' => 'Berhasil keluar, sampai bertemu kembali',
        ],  Response::HTTP_OK);
    }
}
