<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class WithdrawalController extends Controller
{
    public function showReport(Request $request, $id)
    {
        $item = Transaction::find($id);
        return view('pages.invoice.template', compact('item'));
    }
    public function index($id)
    {
        $status = request("status");
        $transactions = Transaction::where("user_id", $id)->where("type", 1)->where("status", $status)->orderBy('created_at', 'desc')->get();
        return response()->json(['message' => "Ok", 'result' => $transactions], Response::HTTP_CREATED);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cash_request' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Input tidak valid', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }

        $wallet = Wallet::where('user_id', $request->user()->id)->first();
        if ($request->cash_request < 10000) {
            return response()->json(['message' => 'Minimal tarik tunai Rp10.000.',], Response::HTTP_UNAUTHORIZED);
        }
        if ($wallet->balance < $request->cash_request) {
            return response()->json(['message' => 'Saldo tidak mencukupi, untuk melakukan tarik tunai.',], Response::HTTP_UNAUTHORIZED);
        }

        $transaction = new Transaction;
        $transaction->invoice_id = "INVWD" . $request->user()->nisn  . now()->timestamp;
        $transaction->user_id  = $request->user()->id;
        $transaction->cash_request  = $request->cash_request;
        $transaction->type  = 1;
        $transaction->status  = 0;
        $transaction->created_at = Carbon::now();
        $transaction->updated_at = Carbon::now();
        $transaction->save();

        return response()->json(['message' => 'Menunggu konfirmasi withdrawal sebesar ' . "Rp" . number_format($request->cash_request, 0, '.', ',') . ", Atas nama " .  $request->user()->name,], Response::HTTP_CREATED);
    }
}
