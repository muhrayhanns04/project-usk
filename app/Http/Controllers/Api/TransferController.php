<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductTransactionResource;
use App\Http\Resources\TransactionResource;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class TransferController extends Controller
{
    public function index($id)
    {
        $status = request("status");
        $type = request("type");
        $transactions = Transaction::where("user_id", $id)->where("type", $type)->where("status", $status)->orderBy('created_at', 'desc')->get();

        if ($type == 3) {
            return response()->json(['message' => "Ok", 'result' => ProductTransactionResource::collection($transactions)], Response::HTTP_CREATED);
        }

        return response()->json(['message' => "Ok", 'result' => TransactionResource::collection($transactions)], Response::HTTP_CREATED);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cash_request' => 'required',
            'confirmed_by' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'Input tidak valid', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }

        $confirmByUser = User::where("nisn", $request->confirmed_by)->first();
        if (!$confirmByUser) {
            return response()->json(['message' => 'Siswa tidak ditemukan pastikan kembali nomer NISN yang dituju'], Response::HTTP_UNAUTHORIZED);
        }

        $myWallet = Wallet::where('user_id', $request->user()->id)->first();
        if ($myWallet->balance < $request->cash_request) {
            return response()->json(['message' => 'Saldo tidak mencukupi, untuk melakukan transfers.',], Response::HTTP_UNAUTHORIZED);
        }

        $transaction = new Transaction;
        $transaction->invoice_id = "INVTF" . $request->user()->nisn  . now()->timestamp;
        $transaction->user_id  = $request->user()->id;
        $transaction->confirmed_by  = $confirmByUser->id;
        $transaction->cash_request  = $request->cash_request;
        $transaction->type  = 4;
        $transaction->status  = 2;
        $transaction->created_at = Carbon::now();
        $transaction->updated_at = Carbon::now();
        $transaction->confirmed_by = $confirmByUser->id;
        $transaction->save();

        $myWallet->balance = $myWallet->balance - $request->cash_request;
        $myWallet->update();

        $theirWallet = Wallet::where("user_id", $confirmByUser->id)->first();
        $theirWallet->balance = $theirWallet->balance + $request->cash_request;
        $theirWallet->update();

        return response()->json(['message' => 'Berhasil transfer uang sebesar ' . "Rp" . number_format($request->cash_request, 0, '.', ',') . ", Ke " .  $confirmByUser->name,], Response::HTTP_CREATED);
    }
}
