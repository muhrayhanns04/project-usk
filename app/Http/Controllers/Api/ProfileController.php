<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProfileResource;
use App\Models\User;
use ErrorException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;
use Symfony\Component\HttpFoundation\Response;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        $user = User::findOrFail($request->user()->id);
        return response()->json(['message' => "Ok", 'result' => ProfileResource::make($user)], Response::HTTP_CREATED);
    }

    public function update(Request $request)
    {
        $user = User::findOrFail($request->user()->id);

        if (strlen($request->old_password) > 1 || $request->old_password !== null) {
            if (!Hash::check($request->old_password, $user->password)) {
                return response()->json(['message' => 'Kata sandi lama tidak cocok',], Response::HTTP_UNAUTHORIZED);
            }
            $user->password = Hash::make($request->new_password);
        }

        $user->update();

        return response()->json(['message' => "Ok", 'result' => ProfileResource::make($user)], Response::HTTP_CREATED);
    }
}
