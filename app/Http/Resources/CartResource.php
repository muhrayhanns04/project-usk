<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product' => [
                'image'   => $this->order->product->image,
                'name'   => $this->order->product->name,
                'qty'   => $this->order->qty,
                'total'   => $this->order->total,
                'confirmed_by'   => $this->confirmed_by,
            ],
            'product_status'   => $this->product_status,
            'created_at'   => $this->created_at,
            'updated_at'   => $this->updated_at,
        ];
    }
}
