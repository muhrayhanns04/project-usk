<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductTransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'invoice_id' => $this->invoice_id,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'confirmed_by' => $this->confirmed_by,
            'created_at' => $this->created_at,
            'order' => [
                'product' => [
                    'name' => $this->order->product->name,
                    'image' => $this->order->product->image,
                ],
                'qty' => $this->order->qty,
                'total' => $this->order->total,
            ],
        ];
    }
}
