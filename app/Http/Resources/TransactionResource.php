<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'invoice_id' => $this->invoice_id,
            'cash_request' => $this->cash_request,
            'user' => [
                'nisn' => $this->user->nisn,
                'name' => $this->user->name,
            ],
            'status' => $this->status,
            'created_at' => $this->created_at,
        ];
    }
}
