<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="shortcut icon" href={{ asset('assets/images/logo/logo-sm.svg') }} type="image/x-icon">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('assets/css/bootstrap.css') }}>
    <link rel="stylesheet" href={{ asset('assets/vendors/bootstrap-icons/bootstrap-icons.css') }}>
    <link rel="stylesheet" href={{ asset('assets/css/app.css') }}>
    <link rel="stylesheet" href={{ asset('assets/css/pages/auth.css') }}>
    <style>
        .background-right {
            height: 100%;
        }

    </style>
</head>

<body>
    <div id="auth">
        <div class="row h-100">
            <div class="col-lg-5 col-12">
                <div id="auth-left">
                    <div class="auth-logo" style="margin-left: 20px ">
                        <a href="index.html"><img src={{ asset('assets/images/logo/logo-large.svg') }}
                                style="transform: scale(2)" alt="Logo"></a>
                    </div>
                    <h1 class="auth-title">Masuk.</h1>
                    <p class="auth-subtitle mb-5">Selamat datang di pusat Billfold!</p>

                    @if ($errors->any())
                        <div class="alert alert-danger border-left-danger" role="alert">
                            <ul class="pl-4 my-2">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        {{-- <div class="form-group position-relative has-icon-left mb-4">
                            <select
                                class="form-control @error('registration_number') is-invalid @enderror form-control-xl"
                                aria-label="Default select example" name="role_id" required>

                                <option selected>Sebagai?</option>
                                <option value="1">Admin</option>
                                <option value="2">Bank</option>
                                <option value="3">Merchant</option>
                            </select>
                            <div class="form-control-icon">
                                <i class="bi bi-person-bounding-box"></i>
                            </div>
                        </div> --}}
                        <div class="form-group position-relative has-icon-left mb-4">
                            <input type="number"
                                class="form-control @error('registration_number') is-invalid @enderror form-control-xl"
                                placeholder="Masukkan Nomer Induk" name="registration_number"
                                value="{{ old('registration_number') }}" required autocomplete="registration_number"
                                autofocus>
                            <div class="form-control-icon">
                                <i class="bi bi-person-check"></i>
                            </div>
                        </div>

                        <div class="form-group position-relative has-icon-left mb-4">
                            <input type="password"
                                class="form-control @error('password') is-invalid @enderror form-control-xl"
                                placeholder="Masukkan Password" name="password" autocomplete="current-password">
                            <div class="form-control-icon">
                                <i class="bi bi-shield-lock"></i>
                            </div>
                        </div>

                        {{-- <div class="form-check form-check-lg d-flex align-items-end">
                            <input class="form-check-input me-2" type="checkbox" name="remember" id="flexCheckDefault"
                                {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label text-gray-600" for="flexCheckDefault">
                                {{ __('Remember Me') }}
                            </label>
                        </div> --}}

                        <button class="btn btn-primary btn-block btn-lg shadow-lg mt-5" type="submit">Log in</button>
                    </form>
                    <div class="text-center mt-5 text-lg fs-4">
                        {{-- <p class="text-gray-600">Don't have an account? <a href="/auth/register" class="font-bold">Sign
                                up</a>.</p> --}}
                        {{-- <p><a class="font-bold" href="{{ route('password.request') }}">Buat akun Guru & Murid ?
                                Register</a>.</p> --}}
                    </div>
                </div>
            </div>
            <div class="col-lg-7 d-none d-lg-block" style="height: 100%;">
                <div id="auth-right" style="height: 100%;">
                    <img src="{{ asset('assets/images/bg/banner.png') }}" class="background-right" alt="">
                </div>
            </div>
        </div>

    </div>
</body>

</html>
