<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="shortcut icon" type="image/jpg" href="https://i.imgur.com/mM4GHMk.png" />

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('assets/css/bootstrap.css') }}>

    <link rel="stylesheet" href={{ asset('assets/vendors/iconly/bold.css') }}>
    <!-- Include Choices CSS -->
    <link rel="stylesheet" href="{{ asset('assets/vendors/choices.js/choices.min.css') }}" />
    <link rel="stylesheet" href={{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.css') }}>
    <link rel="stylesheet" href={{ asset('assets/vendors/bootstrap-icons/bootstrap-icons.css') }}>
    <link rel="stylesheet" href={{ asset('assets/css/app.css') }}>
    <link rel="shortcut icon" href={{ asset('assets/images/logo/logo-sm.svg') }} type="image/x-icon">
    <script src="https://kit.fontawesome.com/dbe540109d.js" crossorigin="anonymous"></script>
    <style>
        #main {
            background-color: #F2F2F2;
        }

    </style>
</head>

<body>

    <div id="app">
        <div id="sidebar" class="active">
            <div class="sidebar-wrapper active">
                <div class="sidebar-header">
                    <div class="d-flex justify-content-between">
                        <div class="logo">
                            <a href="#"><img src="{{ asset('assets/images/logo/logo-large.svg') }}"
                                    class="img-fluid" alt="Logo" srcset=""></a>
                        </div>
                        <div class="toggler">
                            <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                        </div>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class="sidebar-item @if (request()->is('dashboard')) active @endif ">
                            <a href="/" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Dashboard</span>
                                <span
                                    class="text-primary fw-bold">({{ ucfirst(trans(auth()->user()->level)) }})</span>
                            </a>
                        </li>

                        @if (auth()->user()->level === 'admin' || auth()->user()->level === 'bank')
                            <li class="sidebar-item  has-sub @if (request()->is('transaction/topup/*')) active @endif">
                                <a href="#" class='sidebar-link'>
                                    <i class="fas fa-balance-scale"></i>
                                    <span>Topup</span>
                                </a>
                                <ul class="submenu">
                                    <li class="submenu-item text-secondary">
                                        <a href="{{ route('topup') }}" class="text-secondary">Menunggu</a>
                                    </li>
                                    <li class="submenu-item text-secondary">
                                        <a href="{{ route('topup.rejected') }}" class="text-secondary">Ditolak</a>
                                    </li>
                                    <li class="submenu-item text-secondary">
                                        <a href="{{ route('topup.success') }}" class="text-secondary">Berhasil</a>
                                    </li>
                                </ul>
                            </li>
                        @endif

                        @if (auth()->user()->level === 'admin' || auth()->user()->level === 'bank')
                            <li class="sidebar-item  has-sub @if (request()->is('transaction/withdrawal/*')) active @endif">
                                <a href="#" class='sidebar-link'>
                                    <i class="fas fa-arrow-alt-circle-down"></i>
                                    <span>Withdraw</span>
                                </a>
                                <ul class="submenu">
                                    <li class="submenu-item text-secondary">
                                        <a href="/transaction/withdrawal/pending" class="text-secondary">Menunggu</a>
                                    </li>
                                    <li class="submenu-item text-secondary">
                                        <a href="/transaction/withdrawal/rejected" class="text-secondary">Ditolak</a>
                                    </li>
                                    <li class="submenu-item text-secondary">
                                        <a href="/transaction/withdrawal/success" class="text-secondary">Berhasil</a>
                                    </li>
                                </ul>
                            </li>
                        @endif

                        @if (auth()->user()->level === 'admin')
                            <li class="sidebar-item  has-sub @if (request()->is('students/*')) active @endif">
                                <a href="#" class='sidebar-link'>
                                    <i class="fas fa-user-graduate"></i>
                                    <span>Kesiswaan</span>
                                </a>
                                <ul class="submenu">
                                    <li class="submenu-item ">
                                        <a href="{{ route('grade') }}">Kelas</a>
                                    </li>
                                    <li class="submenu-item ">
                                        <a href="{{ route('major') }}">Jurusan</a>
                                    </li>
                                    <li class="submenu-item ">
                                        <a href="{{ route('student') }}">Siswa</a>
                                    </li>
                                </ul>
                            </li>
                        @endif

                        @if (auth()->user()->level === 'admin')
                            <li class="sidebar-item @if (request()->is('bank')) active @endif ">
                                <a href="/bank" class='sidebar-link'>
                                    <i class="fas fa-archway"></i>
                                    <span>Akun Bank</span>
                                </a>
                            </li>
                        @endif

                        @if (auth()->user()->level === 'merchant' || auth()->user()->level === 'admin')
                            <li class="sidebar-item  has-sub @if (request()->is('store/*')) active @endif">
                                <a href="#" class='sidebar-link'>
                                    <i class="fas fa-store"></i>
                                    <span>Toko</span>
                                </a>
                                <ul class="submenu">
                                    @if (auth()->user()->level === 'admin')
                                        <li class="submenu-item ">
                                            <a href="{{ route('merchant.account') }}">Account</a>
                                        </li>
                                    @endif

                                    @if (auth()->user()->level === 'merchant')
                                        <li class="submenu-item ">
                                            <a href="{{ route('merchant.product') }}">Barang</a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif

                        @if (auth()->user()->level === 'merchant')
                            <li class="sidebar-item  has-sub @if (request()->is('product/transaction/*')) active @endif">
                                <a href="#" class='sidebar-link'>
                                    <i class="fas fa-chart-line"></i>
                                    <span>Transaksi</span>
                                </a>
                                <ul class="submenu">
                                    <li class="submenu-item ">
                                        <a href="{{ route('merchant.transaction') }}">Menunggu</a>
                                    </li>
                                    <li class="submenu-item ">
                                        <a href="{{ route('merchant.transaction.rejected') }}">Ditolak</a>
                                    </li>
                                    <li class="submenu-item ">
                                        <a href="{{ route('merchant.transaction.success') }}">Berhasil</a>
                                    </li>
                                </ul>
                            </li>
                        @endif

                        <li class="sidebar-item @if (request()->is('logout')) active @endif ">
                            <a href="{{ route('logout') }}" class='sidebar-link'
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">

                                <i class="bi bi-grid-fill"></i>

                                <span> {{ __('Logout') }}</span>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                class="d-none">
                                @csrf
                            </form>
                        </li>

                    </ul>
                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>

        {{-- Main Content --}}

        @yield('modal')

        <div id="main">
            <header class="mb-3">
                <a href="#" class="burger-btn d-block d-xl-none">
                    <i class="bi bi-justify fs-3"></i>
                </a>
            </header>

            {{-- Yield Content --}}

            @yield('content')
        </div>
    </div>

    @include('sweetalert::alert')

    <script src="{{ asset('assets/vendors/choices.js/choices.min.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src={{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}></script>
    <script src={{ asset('assets/js/bootstrap.bundle.min.js') }}></script>

    <script src={{ asset('assets/vendors/apexcharts/apexcharts.js') }}></script>
    <script src={{ asset('assets/js/pages/dashboard.js') }}></script>
    <script src={{ asset('assets/js/main.js') }}></script>

    @yield('scripts')
</body>

</html>
