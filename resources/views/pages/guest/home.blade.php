@extends('layouts.app')

@section('title', 'Billfold | Dashboard')
@section('page-heading', 'Profile Statistics')

@section('content')
    <div class="page-content">
        {{-- To get username & registration number --}}
        @php
            $level = auth()->user()->nisn;
            $registration_number = '';
            
            switch (auth()->user()->level) {
                case 'student':
                    $registration_number = auth()->user()->nisn;
                    break;
                case 'merchant':
                    $registration_number = auth()->user()->nib;
                    break;
                case 'bank':
                    $registration_number = auth()->user()->nibs;
                    break;
                case 'admin':
                    $registration_number = auth()->user()->nss;
                    break;
            }
            
        @endphp

        <section class="row">
            {{-- Special section for admin & bank --}}
            @if (auth()->user()->level === 'admin' || auth()->user()->level === 'bank')
                <div class="row">

                    <div class="col-6 col-lg-12 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <img src="{{ asset('assets/images/logo/logo-sm.svg') }}" width="60" height="60"
                                        alt="">
                                    <div class="ms-3 name">
                                        <h5 class="font-bold">{{ auth()->user()->name }}</h5>
                                        <h6 class="text-muted mb-0">{{ $registration_number }}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <x-dashboard.info-box title="Keuangan" :value="$bank->balanced" icon="fa-money-bill-wave"
                        bg=" bg-primary" type="1" />
                    <x-dashboard.info-box title="Total Topup" :value="$bank->topup" icon="fa-arrow-alt-circle-down"
                        bg=" bg-primary" type="1" />
                    <x-dashboard.info-box title="Total withdrawal" :value="$bank->withdrawal" icon="fa-arrow-alt-circle-up"
                        bg=" bg-primary" type="1" />
                </div>

                {{-- Special section for admin --}}
                @if (auth()->user()->level === 'admin')
                    <div class="row">
                        <x-dashboard.info-box title="Total Siswa" :value="$students->count()" icon="fa-user-graduate"
                            bg=" bg-primary" type="0" />
                        <x-dashboard.info-box title="Total Pedagang" :value="$merchant->count()" icon="fa-user-astronaut"
                            bg=" bg-primary" type="0" />
                    </div>
                @endif
            @endif

            {{-- Special section for mercant --}}
            @if (auth()->user()->level === 'merchant')
                <div class="row">
                    <div class="col-6 col-lg-12 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <img src="{{ asset('assets/images/logo/logo-sm.svg') }}" width="60" height="60"
                                        alt="">
                                    <div class="ms-3 name">
                                        <h5 class="font-bold">{{ auth()->user()->name }}</h5>
                                        <h6 class="text-muted mb-0">{{ $registration_number }}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <x-dashboard.info-box title="Keuangan" :value="$wallet->balance" icon="fa-money-bill-wave"
                        bg=" bg-primary" type="1" />
                    <x-dashboard.info-box title="Total Pendapatan" :value="$wallet->income" icon="fa-arrow-alt-circle-down"
                        bg=" bg-primary" type="1" />
                </div>
            @endif
        </section>
    </div>

@endsection
