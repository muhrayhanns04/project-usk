@extends('layouts.app')

@section('title', 'Lonceng Indonesia | Transaksi')

@section('modal')
    <div class="modal fade" id="createModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create {{ $title }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" action="{{ route('bank.create') }}" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="something" class="col-form-label">NIBS</label>
                            <input type="number" class="form-control" name="nibs" id="nibs" required>
                        </div>
                        <div class="mb-3">
                            <label for="something" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" name="name" id="name" required>
                        </div>
                        <div class="mb-3">
                            <label for="something" class="col-form-label">Email</label>
                            <input type="email" class="form-control" name="email" id="email" required>
                        </div>


                        <div class="float-end">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Buat</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @foreach ($users as $item)
        <div class="modal fade" id="editModal{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit {{ $title }}
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('bank.update', [$item->id]) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <label for="recipient-name" class="col-form-label">Nomer Induk Bank Sekolah</label>
                                <input type="number" class="form-control" name="nibs" id="nibs"
                                    value="{{ $item->nibs }}" required>
                            </div>
                            <div class="mb-3">
                                <label for="recipient-name" class="col-form-label">Nama</label>
                                <input type="text" class="form-control" name="name" id="name" value="{{ $item->name }}"
                                    required>
                            </div>
                            <div class="mb-3">
                                <label for="recipient-name" class="col-form-label">Email</label>
                                <input type="email" class="form-control" name="email" id="email"
                                    value="{{ $item->email }}" required>
                            </div>
                            <div class="mb-3">
                                <label for="recipient-name" class="col-form-label">Password</label>
                                <input type="text" class="form-control" name="password" id="password"
                                    value="{{ $item->password }}" required>
                            </div>

                            <div class="float-end">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('content')
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="page-heading align-items-center d-flex justify-content-between">
                    <h6 class="text-primary">{{ $title }}</h6>
                    <div class="d-flex align-items-center">
                        <form method="GET" action="{{ route('bank') }}">
                            <input class="btn btn-outline-primary text-left text-md-left my-2 my-sm-0 m-2" type="search"
                                placeholder="Search" name="search" aria-label="Search" />
                        </form>
                        <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal"
                            data-bs-target="#createModal">Create</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">NIBS</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Email</th>
                                <th scope="col">Password Default</th>
                                <th scope="col">Level</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if ($users->count() <= 0)
                                <tr>
                                    <td colspan="6" class="h2 p-4 text-center m-0">Not Found</td>
                                </tr>
                            @endif
                            @foreach ($users as $key => $item)
                                <tr id="item-{{ $item->id }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->nibs }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>siswasmkn10</td>
                                    <td>{{ ucfirst(trans($item->level)) }}</td>
                                    <td class="d-flex align-items-center">
                                        <a href="#" type="button" class="btn btn-primary mx-2" data-bs-toggle="modal"
                                            data-bs-target="#editModal{{ $item->id }}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                                <path
                                                    d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                                            </svg>
                                        </a>

                                        <form action="{{ route('bank.delete', [$item->nibs]) }}" method="GET">
                                            @csrf
                                            <div>
                                                <button type="submit" class="btn btn-danger"><i
                                                        class="fas fa-ban"></i></button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- <div class="d-flex justify-content-between pull-left">
                        <div>
                            Showing
                            {{ $products->firstItem() }}
                            to
                            {{ $products->lastItem() }}
                            of
                            {{ $products->total() }}
                            entries
                        </div>
                        <div class="pull-right">
                            {{ $products->links() }}
                        </div>
                    </div> --}}

                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="{{ asset('js/scripts/pages/produk/index.js') }}" type="module"></script>
@endsection
