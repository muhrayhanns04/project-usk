@extends('layouts.app')

@section('title', 'Lonceng Indonesia | Transaksi')

@section('modal')
    <div class="modal fade" id="createModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create {{ $title }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" action="{{ route('merchant.product.create') }}" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="something" class="col-form-label">Gambar Produk</label>
                            <input type="text" class="form-control" name="image" id="image" required>
                        </div>
                        <div class="mb-3">
                            <label for="something" class="col-form-label">Nama Produk</label>
                            <input type="text" class="form-control" name="name" id="name" required>
                        </div>
                        <div class="mb-3">
                            <label for="something" class="col-form-label">Stock</label>
                            <input type="number" class="form-control" name="stock" id="stock" required>
                        </div>
                        <div class="mb-3">
                            <label for="something" class="col-form-label">Harga</label>
                            <input type="number" class="form-control" name="price" id="price" required>
                        </div>
                        <div class="mb-3">
                            <label for="something" class="col-form-label">Deksripsi</label>
                            <div class="form-floating">
                                <textarea class="form-control" name="description" placeholder="Masukkan deskripsi produk"
                                    id="floatingTextarea"></textarea>
                                <label for="floatingTextarea">Deskripsi</label>
                            </div>
                        </div>

                        <div class="float-end">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Buat</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @foreach ($products as $item)
        <div class="modal fade" id="editModal{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit {{ $title }}
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('merchant.product.update', [$item->id]) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <label for="something" class="col-form-label">Gambar Produk</label>
                                <input type="text" class="form-control" name="image" id="image" required
                                    value="{{ $item->image }}">
                            </div>
                            <div class="mb-3">
                                <label for="something" class="col-form-label">Nama Produk</label>
                                <input type="text" class="form-control" name="name" id="name" required
                                    value="{{ $item->name }}">
                            </div>
                            <div class="mb-3">
                                <label for="something" class="col-form-label">Stock</label>
                                <input type="number" class="form-control" name="stock" id="stock" required
                                    value="{{ $item->stock }}">
                            </div>
                            <div class="  mb-3">
                                <label for="something" class="col-form-label">Harga</label>
                                <input type="number" class="form-control" name="price" id="price" required
                                    value="{{ $item->price }}">
                            </div>
                            <div class="   mb-3">
                                <label for="something" class="col-form-label">Deksripsi</label>
                                <div class="form-floating">
                                    <textarea class="form-control" name="description" placeholder="Masukkan deskripsi produk"
                                        id=" floatingTextarea">{{ $item->description }}</textarea>
                                    <label for="floatingTextarea">Deskripsi</label>
                                </div>
                            </div>

                            <div class="float-end">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('content')
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="page-heading align-items-center d-flex justify-content-between">
                    <h6 class="text-primary">{{ $title }}</h6>
                    <div class="d-flex align-items-center">
                        <form method="GET" action="{{ route('merchant.product') }}">
                            <input class="btn btn-outline-primary text-left text-md-left my-2 my-sm-0 m-2" type="search"
                                placeholder="Search" name="search" aria-label="Search" />
                        </form>
                        <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal"
                            data-bs-target="#createModal">Create</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Produk</th>
                                <th scope="col">Stock</th>
                                <th scope="col">Harga</th>
                                <th scope="col">Deskripsi</th>
                                <th scope="col">Dibuat</th>
                                <th scope="col">Diupdate</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if ($products->count() <= 0)
                                <tr>
                                    <td colspan="6" class="h2 p-4 text-center m-0">Not Found</td>
                                </tr>
                            @endif
                            @foreach ($products as $key => $item)
                                <tr id="item-{{ $item->id }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td class="col-2">
                                        <div class="d-flex align-items-center">
                                            <img src="{{ $item->image ?? asset('assets/images/icons/empty_profile.png') }}"
                                                alt="" width="70" class="mr-3 rounded">
                                            <h6 class="m-0 font-weight-bold m-2">{{ $item->name }}</h6>
                                        </div>
                                    </td>
                                    <td class="{{ $item->stock < 1 ? 'font-bold text-danger' : ' ' }}">
                                        {{ $item->stock < 1 ? 'HABIS' : number_format($item->stock, 0, '.', ',') . 'pcs' }}
                                    </td>
                                    <td>Rp{{ number_format($item->price, 0, '.', ',') }}</td>
                                    <td style="white-space: wrap;overflow: hidden;text-overflow: ellipsis;height: 100px">
                                        {{ $item->description }}</td>
                                    <td class="align-middle" style="white-space: nowrap">
                                        <small class="d-block">{{ $item->created_at->format('d/m/Y') }}</small>
                                        <small class="d-block">{{ $item->created_at->format('H:i') }}</small>
                                    </td>
                                    <td class="align-middle" style="white-space: nowrap">
                                        <small class="d-block">{{ $item->updated_at->format('d/m/Y') }}</small>
                                        <small class="d-block">{{ $item->updated_at->format('H:i') }}</small>
                                    </td>
                                    <td class="align-middle">
                                        <div class="d-flex align-items-center">
                                            <a href="#" type="button" class="btn btn-primary mx-2" data-bs-toggle="modal"
                                                data-bs-target="#editModal{{ $item->id }}">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                    fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                                    <path
                                                        d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                                                </svg>
                                            </a>
                                            <form action="{{ route('merchant.product.delete', [$item->id]) }}"
                                                method="GET">
                                                @csrf
                                                <div>
                                                    <button type="submit" class="btn btn-danger"><i
                                                            class="fas fa-trash"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- <div class="d-flex justify-content-between pull-left">
                        <div>
                            Showing
                            {{ $products->firstItem() }}
                            to
                            {{ $products->lastItem() }}
                            of
                            {{ $products->total() }}
                            entries
                        </div>
                        <div class="pull-right">
                            {{ $products->links() }}
                        </div>
                    </div> --}}

                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="{{ asset('js/scripts/pages/produk/index.js') }}" type="module"></script>
@endsection
