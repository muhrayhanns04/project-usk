@extends('layouts.app')

@section('title', 'Billfold Indonesia | Transaksi')

@section('content')
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="page-heading align-items-center d-flex justify-content-between">
                    <h6 class="text-primary">{{ $title }}</h6>
                    <div class="d-flex align-items-center">
                        <form method="GET" action="{{ route('merchant.transaction') }}">
                            <input class="btn btn-outline-primary text-left text-md-left my-2 my-sm-0 m-2" type="search"
                                placeholder="Search" name="search" aria-label="Search" />
                        </form>
                    </div>
                </div>
                <div class="dataTable-container">
                    <table class="table dataTable-table">
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Download</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Nomer Invoice</th>
                                <th scope="col">Live Preview</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if ($transactions->count() <= 0)
                                <tr>
                                    <td colspan="6" class="h2 p-4 text-center m-0">Not Found</td>
                                </tr>
                            @endif
                            @foreach ($transactions as $key => $item)
                                <tr id="item-{{ $item }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td><a
                                            href="{{ route('merchant.transaction.detailSuccess', [$key]) }}">{{ $key }}</a>
                                    </td>
                                    <td class="col-2">{{ $item[0]->user->name }} <span
                                            class="text-primary font-bold">({{ $item[0]->user->major->grade->name }}
                                            {{ $item[0]->user->major->name }})</span></td>
                                    <td class="align-middle" style="white-space: nowrap">
                                        <small class="d-block">{{ $item[0]->created_at->format('d/m/Y') }}</small>
                                        <small class="d-block">{{ $item[0]->created_at->format('H:i') }}</small>
                                    </td>
                                    <td>
                                        <a href="{{ route('merchant.transaction.report', [$key, 'success']) }}">Lihat
                                            Invoice</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('merchant.transaction.downloadReport', [$key, 'success']) }}">Download
                                            Invoice</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
