@extends('layouts.app')

@section('title', 'Lonceng Indonesia | Topup Dibatalkan')

@section('content')
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="page-heading align-items-center d-flex justify-content-between">
                    <h6 class="text-primary">{{ $title }}</h6>
                    <div class="d-flex align-items-center">
                        <form method="GET" action="{{ route('merchant.transaction') }}">
                            <input class="btn btn-outline-primary text-left text-md-left my-2 my-sm-0 m-2" type="search"
                                placeholder="Search" name="search" aria-label="Search" />
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Produk</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Total</th>
                                <th scope="col">Tipe</th>
                                <th scope="col">Request</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if ($transactions->count() <= 0)
                                <tr>
                                    <td colspan="6" class="h2 p-4 text-center m-0">Not Found</td>
                                </tr>
                            @endif
                            @foreach ($transactions as $key => $item)
                                @php
                                    $statusClass = 'font-weight-bold';
                                    $statusName;

                                    switch ($item->status) {
                                        case '0':
                                            $statusClass .= ' text-warning';
                                            $statusName = 'Menunggu';
                                            break;
                                        case '1':
                                            $statusClass .= ' text-danger';
                                            $statusName = 'Ditolak';
                                            break;
                                        case '2':
                                            $statusClass .= ' text-success';
                                            $statusName = 'Berhasil';
                                            break;
                                    }
                                @endphp
                                @php
                                    $typeStatusClass = 'font-weight-bold';
                                    $type;

                                    switch ($item->type) {
                                        case '0':
                                            $typeStatusClass .= ' text-warning';
                                            $type = 'Topup';
                                            break;
                                        case '1':
                                            $typeStatusClass .= ' text-danger';
                                            $type = 'Withdraw';
                                            break;
                                        case '2':
                                            $typeStatusClass .= ' text-success';
                                            $type = 'Transfer';
                                            break;
                                        case '3':
                                            $typeStatusClass .= ' text-success';
                                            $type = 'Order';
                                            break;
                                    }
                                @endphp
                                <tr id="item-{{ $item->id }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td class="col-2">{{ $item->user->name }} <span
                                            class="text-primary font-bold">({{ $item->user->major->grade->name }}
                                            {{ $item->user->major->name }})</span></td>
                                    <td>{{ $item->order->product->name }}</td>
                                    <td class="col-1">{{ number_format($item->order->qty, 0, '.', ',') }} pcs
                                    </td>
                                    <td class="col-1">Rp{{ number_format($item->order->total, 0, '.', ',') }}
                                    </td>
                                    <td>{{ $type }}</td>
                                    <td class="align-middle" style="white-space: nowrap">
                                        <small class="d-block">{{ $item->created_at->format('d/m/Y') }}</small>
                                        <small class="d-block">{{ $item->created_at->format('H:i') }}</small>
                                    </td>
                                    <td class="align-middle {{ $statusClass }} fw-bold"
                                        title="{{ $item->updated_at->format('d/m/Y H:i') }}">
                                        {{ $statusName }}

                                        @if ($statusName != 'Menunggu')
                                            <small
                                                class="d-block">{{ $item->updated_at->format('d/m/Y H:i') }}</small>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
