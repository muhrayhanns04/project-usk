@extends('layouts.app')

@section('title', 'Lonceng Indonesia | Transaksi')

@section('modal')
    <div class="modal fade" id="createModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create {{ $title }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" action="{{ route('topup.create') }}" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="something" class="col-form-label">Pilih User</label>
                            <div class="form-group">
                                <select class="choices form-select" name="user_id">
                                    <option value="0">Pilih user</option>
                                    @foreach ($users as $key => $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="something" class="col-form-label">Jumlah Topup</label>
                            <input type="number" class="form-control" name="cash_request" id="cash_request">
                        </div>

                        <div class="float-end">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Buat</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="page-heading align-items-center d-flex justify-content-between">
                    <h6 class="text-primary">{{ $title }}</h6>
                    <div class="d-flex align-items-center">
                        <form method="GET" action="{{ route('topup') }}">
                            <input class="btn btn-outline-primary text-left text-md-left my-2 my-sm-0 m-2" type="search"
                                placeholder="Search" name="search" aria-label="Search" />
                        </form>
                        <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal"
                            data-bs-target="#createModal">Create</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Jumlah</th>
                                <th scope="col">Tipe</th>
                                <th scope="col">Request</th>
                                <th scope="col">Status</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if ($transactions->count() <= 0)
                                <tr>
                                    <td colspan="6" class="h2 p-4 text-center m-0">Not Found</td>
                                </tr>
                            @endif
                            @foreach ($transactions as $key => $item)
                                @php
                                    $statusClass = 'font-weight-bold';
                                    $statusName;
                                    
                                    switch ($item->status) {
                                        case '0':
                                            $statusClass .= ' text-warning';
                                            $statusName = 'Menunggu';
                                            break;
                                        case '1':
                                            $statusClass .= ' text-danger';
                                            $statusName = 'Ditolak';
                                            break;
                                        case '2':
                                            $statusClass .= ' text-success';
                                            $statusName = 'Berhasil';
                                            break;
                                    }
                                @endphp
                                @php
                                    $typeStatusClass = 'font-weight-bold';
                                    $type;
                                    
                                    switch ($item->type) {
                                        case '0':
                                            $typeStatusClass .= ' text-warning';
                                            $type = 'Topup';
                                            break;
                                        case '1':
                                            $typeStatusClass .= ' text-danger';
                                            $type = 'Withdraw';
                                            break;
                                        case '2':
                                            $typeStatusClass .= ' text-success';
                                            $type = 'Transfer';
                                            break;
                                        case '3':
                                            $typeStatusClass .= ' text-success';
                                            $type = 'Order';
                                            break;
                                    }
                                @endphp
                                <tr id="item-{{ $item->id }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td><a href="{{ route('topup.report', [$item->id]) }}">{{ $item->user->name }}</a>
                                    </td>
                                    <td>Rp{{ number_format($item->cash_request, 0, '.', ',') }}</td>
                                    <td>{{ $type }}</td>
                                    <td class="align-middle" style="white-space: nowrap">
                                        <small class="d-block">{{ $item->created_at->format('d/m/Y') }}</small>
                                        <small class="d-block">{{ $item->created_at->format('H:i') }}</small>
                                    </td>
                                    <td class="align-middle {{ $statusClass }} fw-bold"
                                        title="{{ $item->updated_at->format('d/m/Y H:i') }}">
                                        {{ $statusName }}

                                        @if ($statusName != 'Menunggu')
                                            <small
                                                class="d-block">{{ $item->updated_at->format('d/m/Y H:i') }}</small>
                                        @endif
                                    </td>
                                    <td class="d-flex align-items-center">
                                        @if ($statusName === 'Menunggu')
                                            <form action="{{ route('topup.approve', [$item->id]) }}" method="POST">
                                                @csrf
                                                <div>
                                                    <button type="submit" class="btn btn-primary m-2"><i
                                                            class="fas fa-check"></i></button>
                                                </div>
                                            </form>

                                            <form action="{{ route('topup.reject', [$item->id]) }}" method="GET">
                                                @csrf
                                                <div>
                                                    <button type="submit" class="btn btn-danger"><i
                                                            class="fas fa-ban"></i></button>
                                                </div>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
