@extends('layouts.app')

@section('title', 'Lonceng Indonesia | Topup Berhasil')

@section('content')
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="page-heading align-items-center d-flex justify-content-between">
                    <h6 class="text-primary">{{ $title }}</h6>
                    <div class="d-flex align-items-center">
                        <form method="GET" action="{{ route('topup') }}">
                            <input class="btn btn-outline-primary text-left text-md-left my-2 my-sm-0 m-2" type="search"
                                placeholder="Search" name="search" aria-label="Search" />
                        </form>
                        <a href="{{ route('topup.reportByFilter') }}" class="btn btn-primary">
                            Laporan Hari Ini
                        </a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Invoice</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Jumlah</th>
                                <th scope="col">Tipe</th>
                                <th scope="col">Request</th>
                                <th scope="col">Status</th>
                                <th scope="col">Live Preview</th>
                                <th scope="col">Download</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if ($transactions->count() <= 0)
                                <tr>
                                    <td colspan="6" class="h2 p-4 text-center m-0">Not Found</td>
                                </tr>
                            @endif
                            @foreach ($transactions as $key => $item)
                                @php
                                    $statusClass = 'font-weight-bold';
                                    $statusName;
                                    
                                    switch ($item->status) {
                                        case '0':
                                            $statusClass .= ' text-warning';
                                            $statusName = 'Menunggu';
                                            break;
                                        case '1':
                                            $statusClass .= ' text-danger';
                                            $statusName = 'Ditolak';
                                            break;
                                        case '2':
                                            $statusClass .= ' text-success';
                                            $statusName = 'Berhasil';
                                            break;
                                    }
                                @endphp
                                @php
                                    $typeStatusClass = 'font-weight-bold';
                                    $type;
                                    
                                    switch ($item->type) {
                                        case '0':
                                            $typeStatusClass .= ' text-warning';
                                            $type = 'Topup';
                                            break;
                                        case '1':
                                            $typeStatusClass .= ' text-danger';
                                            $type = 'Withdraw';
                                            break;
                                        case '2':
                                            $typeStatusClass .= ' text-success';
                                            $type = 'Transfer';
                                            break;
                                        case '3':
                                            $typeStatusClass .= ' text-success';
                                            $type = 'Order';
                                            break;
                                    }
                                @endphp
                                <tr id="item-{{ $item->id }}">
                                    <td>{{ $item->invoice_id }}</td>
                                    <td><a href="{{ route('topup.report', [$item->id]) }}">{{ $item->user->name }}</a>
                                    </td>
                                    <td>Rp{{ number_format($item->cash_request, 0, '.', ',') }}</td>
                                    <td>{{ $type }}</td>
                                    <td class="align-middle" style="white-space: nowrap">
                                        <small class="d-block">{{ $item->created_at->format('d/m/Y') }}</small>
                                        <small class="d-block">{{ $item->created_at->format('H:i') }}</small>
                                    </td>
                                    <td class="align-middle {{ $statusClass }} fw-bold"
                                        title="{{ $item->updated_at->format('d/m/Y H:i') }}">
                                        {{ $statusName }}

                                        @if ($statusName != 'Menunggu')
                                            <small
                                                class="d-block">{{ $item->updated_at->format('d/m/Y H:i') }}</small>
                                        @endif
                                    </td>
                                    <td><a href="{{ route('topup.report', [$item->id]) }}"> Lihat Invoice </a>
                                    <td><a href="{{ route('topup.downloadReport', [$item->id]) }}"> Download Invoice </a>
                                    <td class="d-flex align-items-center">
                                        @if ($statusName === 'Menunggu')
                                            <form action="{{ route('transaction.approve', [$item->id]) }}" method="POST">
                                                @csrf
                                                <div>
                                                    <button type="submit" class="btn btn-primary m-2"><i
                                                            class="fas fa-check"></i></button>
                                                </div>
                                            </form>

                                            <form action="{{ route('transaction.reject', [$item->id]) }}" method="GET">
                                                @csrf
                                                <div>
                                                    <button type="submit" class="btn btn-danger"><i
                                                            class="fas fa-ban"></i></button>
                                                </div>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- <div class="d-flex justify-content-between pull-left">
                        <div>
                            Showing
                            {{ $products->firstItem() }}
                            to
                            {{ $products->lastItem() }}
                            of
                            {{ $products->total() }}
                            entries
                        </div>
                        <div class="pull-right">
                            {{ $products->links() }}
                        </div>
                    </div> --}}

                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="{{ asset('js/scripts/pages/produk/index.js') }}" type="module"></script>
@endsection
