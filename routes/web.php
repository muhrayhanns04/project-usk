<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Web\AuthController;
use App\Http\Controllers\Web\BankController;
use App\Http\Controllers\Web\GradeController;
use App\Http\Controllers\Web\MajorController;
use App\Http\Controllers\Web\ProductController;
use App\Http\Controllers\Web\StoreController;
use App\Http\Controllers\Web\StudentController;
use App\Http\Controllers\Web\TopupController;
use App\Http\Controllers\Web\TransactionController;
use App\Http\Controllers\Web\WithdrawalController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', fn () => redirect('/login'));
Route::middleware(['guest'])->group(function () {
    Route::post('/login', [AuthController::class, 'check'])->name('login');
    Route::get('/login', [AuthController::class, 'loginView'])->name('login.loginView');
    Route::get('/register', [AuthController::class, 'register']);
});


Route::get('invoice/{id}', [TransactionController::class, 'download']);

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index']);

    Route::prefix('/transaction')->group(function () {
        Route::prefix('/topup')->group(function () {
            Route::get('/pending', [TopupController::class, 'index'])->name("topup");
            Route::get('/rejected', [TopupController::class, 'rejectedView'])->name("topup.rejected");
            Route::get('/success', [TopupController::class, 'successView'])->name("topup.success");
            Route::post('/save-create', [TopupController::class, 'store'])->name("topup.create");
            Route::post('/save-edit/{id}', [TopupController::class, 'approve'])->name("topup.approve");
            Route::get('/{id}', [TopupController::class, 'reject'])->name("topup.reject");
            Route::get('/invoice/{id}', [TopupController::class, 'showReport'])->name("topup.report");
            Route::get('/today/invoice', [TopupController::class, 'reportByFilter'])->name("topup.reportByFilter");
            Route::get('/download/{id}', [TopupController::class, 'downloadReport'])->name("topup.downloadReport");
        });
        Route::prefix('/withdrawal')->group(function () {
            Route::get('/pending', [WithdrawalController::class, 'index'])->name("withdrawal");
            Route::get('/rejected', [WithdrawalController::class, 'rejectedView'])->name("withdrawal.rejected");
            Route::get('/success', [WithdrawalController::class, 'successView'])->name("withdrawal.success");
            Route::post('/save-create', [WithdrawalController::class, 'store'])->name("withdrawal.create");
            Route::post('/save-edit/{id}', [WithdrawalController::class, 'approve'])->name("withdrawal.approve");
            Route::get('/{id}', [WithdrawalController::class, 'reject'])->name("withdrawal.reject");
            Route::get('/invoice/{id}', [WithdrawalController::class, 'showReport'])->name("withdrawal.report");
            Route::get('/download/{id}', [WithdrawalController::class, 'downloadReport'])->name("withdrawal.downloadReport");
        });
    });
    Route::prefix('/bank')->group(function () {
        Route::get('/', [BankController::class, 'index'])->name("bank");
        Route::post('/save-create', [BankController::class, 'store'])->name("bank.create");
        Route::post('/save-edit/{id}', [BankController::class, 'update'])->name("bank.update");
        Route::get('/{id}', [BankController::class, 'destroy'])->name("bank.delete");
    });
    Route::prefix("/students")->group(function () {
        Route::prefix('/grade')->group(function () {
            Route::get('/', [GradeController::class, 'index'])->name("grade");
            Route::post('/save-create', [GradeController::class, 'store'])->name("grade.create");
            Route::post('/save-edit/{id}', [GradeController::class, 'update'])->name("grade.update");
            Route::get('/{id}', [GradeController::class, 'destroy'])->name("grade.delete");
        });
        Route::prefix('/major')->group(function () {
            Route::get('/', [MajorController::class, 'index'])->name("major");
            Route::post('/save-create', [MajorController::class, 'store'])->name("major.create");
            Route::post('/save-edit/{id}', [MajorController::class, 'update'])->name("major.update");
            Route::get('/{id}', [MajorController::class, 'destroy'])->name("major.delete");
        });
        Route::prefix('/student')->group(function () {
            Route::get('/', [StudentController::class, 'index'])->name("student");
            Route::post('/save-create', [StudentController::class, 'store'])->name("student.create");
            Route::post('/save-edit/{id}', [StudentController::class, 'update'])->name("student.update");
            Route::get('/{id}', [StudentController::class, 'destroy'])->name("student.delete");
        });
    });

    Route::prefix('/store')->group(function () {
        Route::prefix('/account')->group(function () {
            Route::get('/', [StoreController::class, 'viewAccount'])->name("merchant.account");
            Route::post('/save-create', [StoreController::class, 'accountStore'])->name("merchant.account.create");
            Route::post('/save-edit/{id}', [StoreController::class, 'accountUpdate'])->name("merchant.account.update");
            Route::get('/{id}', [StoreController::class, 'accountDestroy'])->name("merchant.account.delete");
        });
        Route::prefix('/product')->group(function () {
            Route::get('/', [ProductController::class, 'index'])->name("merchant.product");
            Route::post('/save-create', [ProductController::class, 'store'])->name("merchant.product.create");
            Route::post('/save-edit/{id}', [ProductController::class, 'update'])->name("merchant.product.update");
            Route::get('/{id}', [ProductController::class, 'destroy'])->name("merchant.product.delete");
        });
    });
    Route::prefix('/product')->group(function () {
        Route::prefix('/transaction')->group(function () {
            Route::get('/pending', [TransactionController::class, 'pendingView'])->name("merchant.transaction");

            Route::get('/detail/{invoice_id}', [TransactionController::class, 'detailPendingView'])->name("merchant.transaction.detail");
            Route::get('/detail-rejected/{invoice_id}', [TransactionController::class, 'detailRejectedView'])->name("merchant.transaction.detailRejected");
            Route::get('/detail-success/{invoice_id}', [TransactionController::class, 'detailSuccessView'])->name("merchant.transaction.detailSuccess");

            Route::get('/rejected', [TransactionController::class, 'rejectedView'])->name("merchant.transaction.rejected");
            Route::get('/success', [TransactionController::class, 'successView'])->name("merchant.transaction.success");
            Route::post('/save-edit/{id}', [TransactionController::class, 'approve'])->name("merchant.transaction.approve");
            Route::post('/approve-all/{id}', [TransactionController::class, 'approveAll'])->name("merchant.transaction.approveAll");
            Route::get('/{id}', [TransactionController::class, 'reject'])->name("merchant.transaction.reject");
            Route::get('/unrejected/{id}', [TransactionController::class, 'unrejected'])->name("merchant.transaction.unrejected");

            Route::get('/invoice/{id}/{status}', [TransactionController::class, 'showReport'])->name("merchant.transaction.report");
            Route::get('/download/{id}/{status}', [TransactionController::class, 'downloadReport'])->name("merchant.transaction.downloadReport");
        });
    });
});
