<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\TopupController;
use App\Http\Controllers\Api\TransactionController;
use App\Http\Controllers\Api\TransferController;
use App\Http\Controllers\Api\WithdrawalController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Protected routes
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/transaction/invoice/{id}', [TransactionController::class, 'download']);
    Route::prefix('/transaction')->group(function () {
        Route::get('/oncart', [TransactionController::class, 'onCart']);
        Route::get('/cartcount', [TransactionController::class, 'cartCount']);
        Route::post('/checkout', [TransactionController::class, 'checkout']);
        Route::get('/total', [TransactionController::class, 'totalPayment']);
        // Route::post('/order/update', [TransactionController::class, 'updateQuantity']);
        Route::post('/order/{id}', [TransactionController::class, 'addToCart']);
        Route::post('/order/delete/{id}', [TransactionController::class, 'destroy']);
        Route::get('/{id}', [TransactionController::class, 'index']);
        Route::get('/history/{id}', [TransferController::class, 'index']);
        Route::prefix("/topup")->group(function () {
            Route::post('/', [TopupController::class, 'store']);
            Route::get('/{id}', [TopupController::class, 'index']);
        });
        Route::prefix("/transfer")->group(function () {
            Route::post('/', [TransferController::class, 'store']);
        });
        Route::prefix("/withdrawal")->group(function () {
            Route::post('/', [WithdrawalController::class, 'store']);
            Route::get('/{id}', [WithdrawalController::class, 'index']);
        });
    });

    Route::get("/products", [ProductController::class, "index"]);

    Route::prefix('/profile')->group(function () {
        Route::get("/", [ProfileController::class, "index"]);
        Route::post("/", [ProfileController::class, "update"]);
    });
});

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/', function () {
    return response()->json(['message' => 'Documentaion API Lonceng Indonesia ©️ Hans'],);
});
