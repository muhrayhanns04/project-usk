<?php

namespace Database\Seeders;

use App\Models\Wallet;
use Illuminate\Database\Seeder;

use function GuzzleHttp\Promise\each;

class WalletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                "user_id" => 3,
                "balance" => 0,
                "income" => 0
            ],
            [
                "user_id" => 1,
                "balance" => 0,
                "income" => 0
            ],
        ])->each(function ($wallet) {
            Wallet::create($wallet);
        });
    }
}
