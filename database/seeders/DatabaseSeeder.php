<?php

namespace Database\Seeders;

use App\Models\MajorTeacher;
use App\Models\Nik;
use App\Models\PrecenseTime;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([BankSeeder::class]);
        $this->call([GradeSeeder::class]);
        $this->call([MajorSeeder::class]);
        collect(
            [
                [
                    'nisn' => '0040354855',
                    'name' => "Muhammad Rayhan",
                    'email' => "studywithray04@gmail.com",
                    'password' => Hash::make("password"),
                    'major_id' => 1,
                    'level' => "student",
                ],
                [
                    'nss' => "34101016406500",
                    'name' => "Admin SMKN10",
                    'email' => "admin10@gmail.com",
                    'password' => Hash::make("password"),
                    'level' => "admin",
                ]
            ]
        )->each(function ($user) {
            User::create($user);
        });
        $this->call([UserSeeder::class]);
        $this->call([MajorTeacherSeeder::class]);
        $this->call([WalletSeeder::class]);
    }
}
