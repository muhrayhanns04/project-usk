<?php

namespace Database\Seeders;

use App\Models\MajorTeacher;
use Illuminate\Database\Seeder;

class MajorTeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                "grade_id" => 3,
                "name" => "RPL",
                "user_id" => 3
            ],
            [
                "grade_id" => 2,
                "name" => "AKL-2",
                "user_id" => 4
            ],
            [
                "grade_id" => 1,
                "name" => "OTP-1",
                "user_id" => 4
            ],
            [
                "grade_id" => 1,
                "name" => "BDP-2",
                "user_id" => 3
            ],
        ])->each(function ($major) {
            MajorTeacher::create($major);
        });
    }
}
