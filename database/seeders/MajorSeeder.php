<?php

namespace Database\Seeders;

use App\Models\Major;
use Illuminate\Database\Seeder;

class MajorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                "grade_id" => 3,
                "name" => "RPL",
            ],
            [
                "grade_id" => 3,
                "name" => "AKL-1",
            ],
            [
                "grade_id" => 3,
                "name" => "AKL-2",
            ],
            [
                "grade_id" => 3,
                "name" => "OTP-1",
            ],
            [
                "grade_id" => 3,
                "name" => "OTP-2",
            ],
            [
                "grade_id" => 3,
                "name" => "BDP-1",
            ],
            [
                "grade_id" => 3,
                "name" => "BDP-2",
            ],

            [
                "grade_id" => 2,
                "name" => "RPL",
            ],
            [
                "grade_id" => 2,
                "name" => "AKL-1",
            ],
            [
                "grade_id" => 2,
                "name" => "AKL-2",
            ],
            [
                "grade_id" => 2,
                "name" => "OTP-1",
            ],
            [
                "grade_id" => 2,
                "name" => "OTP-2",
            ],
            [
                "grade_id" => 2,
                "name" => "BDP-1",
            ],
            [
                "grade_id" => 2,
                "name" => "BDP-2",
            ],

            [
                "grade_id" => 1,
                "name" => "RPL",
            ],
            [
                "grade_id" => 1,
                "name" => "AKL-1",
            ],
            [
                "grade_id" => 1,
                "name" => "AKL-2",
            ],
            [
                "grade_id" => 1,
                "name" => "OTP-1",
            ],
            [
                "grade_id" => 1,
                "name" => "OTP-2",
            ],
            [
                "grade_id" => 1,
                "name" => "BDP-1",
            ],
            [
                "grade_id" => 1,
                "name" => "BDP-2",
            ],
        ])->each(function ($major) {
            Major::create($major);
        });
    }
}
