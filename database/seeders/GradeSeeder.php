<?php

namespace Database\Seeders;

use App\Models\Grade;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                "name" => "X",
            ], [
                "name" => "XI",
            ], [
                "name" => "XII",
            ]
        ])->each(function ($grade) {
            Grade::create($grade);
        });
    }
}
