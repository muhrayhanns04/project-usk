<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([[
            'nib' => '9041253455',
            'name' => "Merchant 1",
            'email' => "merchant@gmail.com",
            'password' => Hash::make("password"),
            'level' => "merchant",
        ], [
            'nibs' => '321023123',
            'name' => "Bank",
            'email' => "bank@gmail.com",
            'password' => Hash::make("password"),
            'level' => "bank",
        ]])->each(function ($user) {
            User::create($user);
        });
    }
}
