<?php

namespace Database\Seeders;

use App\Models\Bank;
use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                "balanced" => 0,
                "topup" => 0,
                "withdrawal" => 0
            ],
        ])->each(function ($item) {
            Bank::create($item);
        });
    }
}
