<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nisn')->nullable();
            $table->string('nib')->nullable(); //nomer induk berusaha
            $table->string('nibs')->nullable(); //nomer induk bank sekolah
            $table->string('nss')->nullable(); //nomer induk sekolah
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->foreignId("major_id")->nullable()->constrained("majors")->onUpdate("cascade")->onDelete("cascade");
            $table->enum("level", ["student", "admin", "bank", "merchant"])->default("student");
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
