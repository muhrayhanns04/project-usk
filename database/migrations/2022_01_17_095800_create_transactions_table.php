<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string("invoice_id")->nullable();
            $table->foreignId("user_id")->constrained("users")->onDelete("cascade")->onUpdate("cascade");
            $table->bigInteger("cash_request")->nullable();
            $table->string("description")->nullable();
            $table->foreignId("order_id")->nullable()->constrained("orders")->onDelete("cascade")->onUpdate("cascade");
            $table->smallInteger("type")->default("0"); //0 = topup, 1 = withdrawal, 2 = transfer, 3 = order, 4 transfer
            $table->smallInteger("status")->default("0"); // 0 = pending, 1 = rejected, 2 = success
            $table->smallInteger("product_status")->nullable(); // 0 = not on the cart, 1 = on cart the cart
            $table->foreignId("confirmed_by")->nullable()->constrained("users")->onDelete("cascade")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
